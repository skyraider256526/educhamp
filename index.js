const express = require("express"),
  passport = require("passport"),
  cors = require("cors"),
  path = require("path");

// Access to .env variables
require("dotenv").config();

const app = express();

// Configures the database and opens an global connection for all modules
require("./config/database");

// Configuring the passport strategy
require("./config/passport");

// Initialize passport for every request
app.use(passport.initialize());

// Allows React to make HTTP requests
app.use(cors());

if (process.env.NODE_ENV !== "production") {
  // Logging all requests for development
  app.use(require("morgan")("dev"));
  app.get("/test", (req, res) => {
    res.send("Server working");
  });
}

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use("/api/auth", require("./routes/auth"));
// app.use("/api/question", require("./routes/question"));

app.use("/api", require("./routes"));

if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log("Server is Running..."));
