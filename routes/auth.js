const router = require("express").Router(),
  bcrypt = require("bcrypt"),
  validateSignup = require("../utils/validation").validateSignup,
  issueJWT = require("../utils").issueJWT;
User = require("../models/User");

/**
 * @route POST /api/auth/login
 *  Validate an existing user and issue a JWT
 */
router.post("/login", function (req, res, next) {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (!user)
        return res
          .status(401)
          .json({ message: `No user found with email ${req.body.email}` });
      if (!user.password)
        return res.status(400).json({
          message: `User is not signed up with password for ${email}. Please try Google sign in.`,
        });
      bcrypt.compare(req.body.password, user.password).then(result => {
        if (result) {
          const { token, expires } = issueJWT(user);
          return res.status(200).json({
            token: token,
            expiresIn: expires,
            user: {
              email: user.email,
              username: user.username,
            },
          });
        } else {
          return res.status(401).json({ message: "Incorrect Password" });
        }
      });
    })
    .catch(err => next(err));

  // User.findOne({ email: req.body.email })
  //   .then(user => {
  //     if (!user) {
  //       res.status(401).json({ success: false, msg: "could not find user" });
  //     }

  //     // Function defined at bottom of app.js
  //     const isValid = utils.validPassword(
  //       req.body.password,
  //       user.hash,
  //       user.salt
  //     );

  //     if (isValid) {
  //       const tokenObject = issueJWT(user);

  //       res.status(200).json({
  //         success: true,
  //         token: tokenObject.token,
  //         expiresIn: tokenObject.expires,
  //       });
  //     } else {
  //       res
  //         .status(401)
  //         .json({ success: false, msg: "you entered the wrong password" });
  //     }
  //   })
  //   .catch(err => {
  //     res.status(400).json(err);
  //   });
});

/**
 * @route POST /api/auth/signup
 * @desc Register a new user,
 */
router.post("/signup", function (req, res) {
  const { error, value } = validateSignup.validate(req.body);
  if (error) return res.status(400).json(error);
  User.findOne({ email: value.email })
    .then(user => {
      if (user) {
        return res.status(409).json({ message: "User already exist" });
      }
      const newUser = new User({
        username: value.username,
        email: value.email,
        password: value.password,
      });

      // Mongoose pre.save middleware will hash the password
      newUser
        .save()
        .then(user => {
          const { token, expires } = issueJWT(user);

          res.status(200).json({
            token: token,
            expiresIn: expires,
            user: {
              email: user.email,
              username: user.username,
            },
          });
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
  // const newUser = new User({
  //   username: req.body.username,
  //   email: req.body.email,
  //   password: req.body.password,
  // });

  // try {
  //   newUser.save().then(user => {
  //     res.json({ success: true, user: user });
  //   });
  // } catch (err) {
  //   res.json({ success: false, msg: err });
  // }
});

module.exports = router;
