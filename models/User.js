const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;

/**
 * @description This is the user schema
 */
const userSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  registered_date: {
    type: Date,
    default: Date.now,
  },
});

// Mongoose middleware to hash the password before saving, even when the password is modified
userSchema.pre("save", function (next) {
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt
    .hash(this.password, 10)
    .then(password => {
      this.password = password;
      next();
    })
    .catch(err => next(err));
  //   bcrypt.genSalt(10, (err, salt) => {
  //     if (err) return next(err);
  //     bcrypt.hash(this.password, salt, (err, hash) => {
  //       if (err) return next(err);
  //       this.password = hash;
  //       next();
  //     });
  //   });
});

const User = mongoose.model("users", userSchema);
module.exports = User;
