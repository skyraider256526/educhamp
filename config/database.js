const mongoose = require("mongoose");

require("dotenv").config();

/**
 * @constant {string} - Url to the database
 */
const DB_URL = process.env.DB_STRING.replace(
  "<password>",
  process.env.DB_PASSWORD
);

mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connected to database"))
  .catch(err => console.log(`Failed to connect to database: ${err}`));
