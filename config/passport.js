const fs = require("fs"),
  path = require("path"),
  passport = require("passport"),
  User = require("./../models/User"),
  JwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt;

/**
 * @constant {string} pathToKey - The path to public key
 */
const pathToKey = path.join(__dirname, "..", "id_rsa_pub.pem");

/**
 * @constant {string} PUB_KEY - The public key
 */
const PUB_KEY = fs.readFileSync(pathToKey, "utf-8");

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: PUB_KEY,
  algorithms: ["RS256"],
};

const strategy = new JwtStrategy(options, (payload, done) => {
  User.findOne({ _id: payload.sub })
    .then(user => {
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    })
    .catch(err => done(err));
});

passport.use(strategy);

// Passport local strategy, signin requires email and password and return JWT Token
// passport.use(
//   new LocalStrategy(
//     {
//       usernameField: "email",
//     },
//     function (email, password, done) {
//       User.findOne({ email: email })
//         .then(user => {
//           if (!user)
//             return done(null, false, {
//               message: `No user found with email ${email}`,
//             });
//           if (!user.password)
//             return done(null, false, {
//               message: `User is not signed up with password for ${email}. Please try Google sign in.`,
//             });

//           bcrypt.compare(password, user.password, (err, isMatch) => {
//             if (err) throw err;

//             if (isMatch) {
//               return done(null, user);
//             } else {
//               return done(null, false, { message: "Incorrect password" });
//             }
//           });
//         })
//         .catch(err => done(err));
//     }
//   )
// );
