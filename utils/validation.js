const Joi = require("joi");
/**
 * @desc Joi schema to validate incoming POST request (used in auth router).
 */
exports.validateSignup = Joi.object({
  username: Joi.string().min(3).max(30).required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

exports.validateLogin = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});
