/**
 * @description Shuffles array using Fisher-Yates shuffle
 * @param  {Array} array - array which needs to be shuffled
 * @returns {Array} array - the shuffled array
 */
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
    [array[i], array[j]] = [array[j], array[i]];
  }
}

module.exports = shuffle;
